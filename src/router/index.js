import { createRouter, createWebHistory } from 'vue-router'
import Home from '../views/Home.vue'
import LoginAgricultor from '../views/LoginAgricultor.vue'
import LoginCliente from '../views/LoginCliente.vue'
import Finca from '../views/Finca.vue'
import SelectProduct from '../views/SelectProduct.vue'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/loginAgricultor',
    name: 'LoginAgricultor',
    component: LoginAgricultor
  },
  {
    path: '/loginCliente',
    name: 'LoginCliente',
    component: LoginCliente
  },
  {
    path: '/finca',
    name: 'Finca',
    component: Finca
  },
  {
    path: '/selectProduct',
    name: 'SelectProduct',
    component: SelectProduct
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
